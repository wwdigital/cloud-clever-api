const mongoose = require('mongoose');

const userSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
      lowercase: true,
    },
    phone: {
      type: String,
      required: false,
    },
    password: {
      type: String,
      required: false,
    },
    note: {
      type: String,
      required: false,
    },
    barCode: {
      type: String,
      required: false,
    },
    address: {
      type: String,
      required: false,
    },
    image: {
      type: String,
      required: false,
    },
    country: {
      type: String,
      required: false,
    },
    city: {
      type: String,
      required: false,
    },
    dateOfBirth: {
      type: String,
      required: false,
    },
    newsletter: {
      type: Boolean,
      default: false,
    },
    company: {
      type: String,
      required: false,
    },
    street: {
      type: String,
      required: false,
    },
    APE: {
      type: String,
      required: false,
    },
    website: {
      type: String,
      required: false,
    },
    // customer_type: {
    //   type: String,
    //   default: 'Pos Customer',
    //   enum: ['Visitor', 'Guest', 'Customer', 'POS Customer'],
    // },
  },
  {
    timestamps: true,
  }
);

// const User = mongoose.models.User || mongoose.model('User', userSchema);

module.exports = userSchema;
