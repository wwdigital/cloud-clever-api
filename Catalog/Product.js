const mongoose = require('mongoose');

const productSchema = new mongoose.Schema(
  {
    productId: {
      type: String,
      required: false,
    },
    sku: {
      type: String,
      required: false,
    },
    barcode: {
      type: String,
      required: false,
    },
    title: {
      type: String,
      required: true,
    },
    slug: {
      type: String,
      required: true,
    },

    categories: {
      type: Array,
      required: true,
    },
    category: {
      type: String,
      required: true,
    },
    image: {
      type: Array,
      required: true,
    },
    originalPrice: {
      type: Number,
      required: false,
    },
    price: {
      type: Number,
      required: false,
    },
    costPrice: {
      type: Number,
      required: false,
    },
    discount: {
      type: Number,
      required: false,
    },
    quantity: {
      type: Number,
      required: false,
    },
    stock: {
      type: Number,
      required: false,
    },

    variants: [{}],

    description: {
      type: String,
      required: true,
    },
    type: {
      type: String,
      required: false,
    },
    tag: [String],

    flashSale: {
      type: Boolean,
      required: false,
      default: false,
    },
    show: {
      type: Array,
      default: ['pos', 'store'],
    },
    status: {
      type: String,
      default: 'show',
      lowercase: true,
      enum: ['show', 'hide'],
    },
  },

  {
    timestamps: true,
  }
);

// const Product = mongoose.model('Product', productSchema);

module.exports = productSchema;
// module.exports = Product;
