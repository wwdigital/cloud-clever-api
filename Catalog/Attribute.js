const mongoose = require('mongoose');

const attributeSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
    },
    name: {
      type: String,
      required: true,
    },
    variants: [{}],
    option: {
      type: String,
      enum: ['Dropdown', 'Radio'],
    },
    status: {
      type: String,
      lowercase: true,
      enum: ['show', 'hide'],
      default: 'show',
    },
  },
  {
    timestamps: true,
  }
);

// const Attribute = mongoose.model('Attribute', attributeSchema);

module.exports = attributeSchema;
// module.exports = Attribute;
