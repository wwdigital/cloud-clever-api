const mongoose = require('mongoose');

const stateSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    iso_code: {
      type: String,
      required: true,
    },
    country: {
      type: String,
      required: true,
    },
    zone: {
      type: String,
      required: true,
    },
    state_code: {
      type: String,
      required: false,
    },
    tax_behavior: {
      type: Number,
      required: false,
    },
    status: {
      type: String,
      default: 'hide',
      lowercase: true,
      enum: ['show', 'hide'],
    },
  },
  {
    timestamps: true,
  }
);
// const State = mongoose.model('State', stateSchema);

module.exports = stateSchema;
