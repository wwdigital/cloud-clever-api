const mongoose = require('mongoose');

const languageSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    iso_code: {
      type: String,
      required: true,
    },
    language_code: {
      type: String,
      required: true,
    },
    date_format: {
      type: String,
      required: false,
    },
    date_format_full: {
      type: String,
      required: false,
    },
    flag: {
      type: String,
      required: false,
    },
    status: {
      type: String,
      lowercase: true,
      enum: ['show', 'hide'],
      default: 'show',
    },
  },
  {
    timestamps: true,
  }
);

module.exports = languageSchema;
