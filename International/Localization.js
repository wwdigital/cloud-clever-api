const mongoose = require('mongoose');

const localizationSchema = new mongoose.Schema(
  {
    weight_unit: {
      type: String,
      required: true,
    },
    distance_unit: {
      type: String,
      required: true,
    },
    volume_unit: {
      type: String,
      required: true,
    },
    volume_unit: {
      type: String,
      required: false,
    },
    dimension_unit: {
      type: String,
      required: true,
    },
    
  },
  {
    timestamps: true,
  }
);

module.exports = localizationSchema;
