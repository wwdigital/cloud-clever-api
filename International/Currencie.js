const mongoose = require("mongoose");

const currencieSchema = new mongoose.Schema(
  {
    symbol: {
      type: String,
      required: true,
    },
    iso_code: {
      type: String,
      required: true,
    },
    exchange_rate: {
      type: String,
      required: true,
    },
    enabled: {
      type: String,
      lowercase: true,
      enum: ["show", "hide"],
      default: "show",
    },
    live_exchange_rates: {
      type: String,
      lowercase: true,
      enum: ["show", "hide"],
      default: "show",
    },
  },
  {
    timestamps: true,
  }
);

module.exports = currencieSchema;
