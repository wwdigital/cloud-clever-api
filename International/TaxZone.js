const mongoose = require('mongoose');

const taxZoneSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    zones: [{}],
    countries: {
      type: String,
      required: true,
    },
    states: [{}],
    tax_rate: {
      type: String,
      required: true,
    },
    status: {
      type: String,
      enum: ['show', 'hide'],
    },
  },
  {
    timestamps: true,
  }
);

module.exports = taxZoneSchema;
