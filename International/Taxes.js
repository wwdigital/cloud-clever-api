const mongoose = require('mongoose');

const taxesSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    tax_rate: {
      type: String,
      required: true,
    },
    country_code: {
      type: String,
      required: false,
    },
    status: {
      type: String,
      default: 'hide',
      enum: ['show', 'hide'],
    },
  },
  {
    timestamps: true,
  }
);

// const Tax = mongoose.model('Tax', taxesSchema);
module.exports = taxesSchema;
