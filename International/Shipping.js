const mongoose = require('mongoose');

const shippingSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    carrier_logo: {
      type: String,
      required: false,
    },
    description: {
      type: String,
      required: true,
    },
    shipping_rate: {
      type: Number,
      required: true,
    },
    status: {
      type: String,
      enum: ['show', 'hide'],
    },
    free_shipping: {
      type: Boolean,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = shippingSchema;
