const mongoose = require('mongoose');

const countrySchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    iso_code: {
      type: String,
      required: true,
    },
    call_prefix: {
      type: String,
      required: true,
    },
    currency: {
      type: String,
      required: false,
    },
    zone: {
      type: String,
      required: true,
    },
    status: {
      type: String,
      lowercase: true,
      enum: ['show', 'hide'],
      default: 'show',
    },
  },
  {
    timestamps: true,
  }
);

// const Country = mongoose.model('Country', countrySchema);

module.exports = countrySchema;
