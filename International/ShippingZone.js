const mongoose = require('mongoose');

const shippingZoneSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    zones: [{}],
    countries: {
      type: String,
      required: true,
    },
    states: [{}],
    shipping_rate: {
      type: String,
      required: true,
    },
    status: {
      type: String,
      enum: ['show', 'hide'],
    },
  },
  {
    timestamps: true,
  }
);

module.exports = shippingZoneSchema;
