const mongoose = require('mongoose');

const customerSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    phone: {
      type: Number,
      required: true,
    },
    note: {
      type: String,
      required: false,
    },
    barcode: {
      type: String,
      required: false,
    },
    password: {
      type: String,
      required: false,
    },
    birth: {
      type: String,
      required: false,
    },
    company: {
      type: String,
      required: false,
    },
    street: {
      type: String,
      required: false,
    },
    ape: {
      type: String,
      required: false,
    },
    website: {
      type: String,
      required: false,
    },
  },

  {
    timestamps: true,
  }
);

// const Customer = mongoose.model('Customer', customerSchema);

module.exports = customerSchema;
