const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

const adminSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    cname: {
      // CCDev - Adding cname to mongo
      type: String,
      required: false,
      unique: false,
      lowercase: true,
    },

    email: {
      type: String,
      required: true,
      // unique: true,
      lowercase: true,
    },

    password: {
      type: String,
      required: false,
      default: bcrypt.hashSync('12345678'),
    },
    role: {
      type: String,
      default: 'Admin',
      enum: ['Admin', 'Superadmin', 'Cashier'],
    },

    type: {
      type: String,
      required: false,
    },
    userid: {
      type: Number,
      required: false,
    },
    pid: {
      type: Number,
      required: false,
    },
    userstatus: {
      type: String,
      required: false,
      default: 'Inactive',
      enum: ['Active', 'Inactive'],
    },
    whmcsstatus: {
      type: String,
      required: false,
    },
    clientdetails: {
      type: String,
      required: false,
    },

    phone: {
      type: String,
      required: false,
    },

    image: {
      type: String,
      required: false,
    },
    address: {
      type: String,
      required: false,
    },
    country: {
      type: String,
      required: false,
    },
    city: {
      type: String,
      required: false,
    },
    joiningData: {
      type: Date,
      required: false,
    },
    shop_status: {
      type: String,
      default: 'Active',
      enum: ['Active', 'Inactive', 'Suspend'],
    },
    pos_status: {
      type: String,
      default: 'Active',
      enum: ['Active', 'Inactive', 'Suspend'],
    },
    site_status: {
      type: String,
      default: 'Active',
      enum: ['Active', 'Inactive', 'Suspend'],
    },
  },
  {
    timestamps: true,
  }
);

// const Admin = mongoose.models.Admin || mongoose.model('Admin', adminSchema);

module.exports = adminSchema;
