const mongoose = require('mongoose');

const zoneSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    status: {
      type: String,
      default: 'hide',
      enum: ['show', 'hide'],
    },
  },
  {
    timestamps: true,
  }
);

// const Zone = mongoose.model('Zone', zoneSchema);

module.exports = zoneSchema;
