const mongoose = require("mongoose");

//pos setting schema
const posSettingSchema = new mongoose.Schema(
  {
    shop_name: {
      type: String,
      required: true,
    },
    company_name: {
      type: String,
      required: false,
    },
    vat_number: {
      type: Number,
      required: false,
    },

    logo: {
      type: String,
      required: false,
    },
    shop_address_one: {
      type: String,
      required: true,
    },
    shop_address_two: {
      type: String,
      required: false,
    },
    shop_address_three: {
      type: String,
      required: false,
    },
    contact_one: {
      type: String,
      required: true,
    },
    contact_two: {
      type: String,
      required: false,
    },
    email: {
      type: String,
      required: true,
    },
    web_site: {
      type: String,
      required: true,
    },
    slip_note: {
      type: String,
      required: false,
    },
    show_images: {
      type: Boolean,
      required: true,
    },
    pos_customer: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    allow_out_of_stock: {
      type: Boolean,
      required: true,
    },
    payment_option: {
      type: Boolean,
      required: true,
    },
    payment_method: {
      type: String,
      required: true,
    },
    allow_discount: {
      type: Boolean,
      required: true,
    },
    print_slip: {
      type: Boolean,
      required: true,
    },
    email_slip: {
      type: Boolean,
      required: true,
    },
    currency_symbol: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

//online setting schema
const onlineStoreSettingSchema = new mongoose.Schema(
  {
    shop_activity: {
      type: String,
      required: true,
    },
    web_site: {
      type: String,
      required: false,
    },
    allow_out_of_stock: {
      type: Boolean,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

//global setting schema
const globalSettingSchema = new mongoose.Schema(
  {
    number_of_image_per_product: {
      type: Number,
      required: true,
    },
    multiple_language: {
      type: Boolean,
      required: true,
    },
    multiple_currencies: {
      type: Boolean,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

const settingSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    setting: [{}],
  },
  {
    timestamps: true,
  }
);

module.exports = settingSchema;
