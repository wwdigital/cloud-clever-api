const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      required: true,
    },
    invoice: {
      type: Number,
      required: false,
    },
    cart: [{}],
    name: {
      type: String,
      required: false,
    },
    email: {
      type: String,
      required: false,
    },
    contact: {
      type: String,
      required: false,
    },
    address: {
      type: String,
      required: false,
    },
    city: {
      type: String,
      required: false,
    },
    country: {
      type: String,
      required: false,
    },
    zipCode: {
      type: String,
      required: false,
    },
    subTotal: {
      type: Number,
      required: true,
    },
    shippingCost: {
      type: Number,
      required: true,
    },
    discount: {
      type: Number,
      required: true,
      default: 0,
    },
    total: {
      type: Number,
      required: true,
    },
    dueAmount: {
      type: Number,
      required: false,
      default: 0,
    },
    returnAmount: {
      type: Number,
      required: false,
      default: 0,
    },
    paidAmount: {
      type: Number,
      required: true,
      default: 0,
    },
    customerNote: {
      type: String,
      required: false,
    },
    shippingOption: {
      type: String,
      required: false,
    },
    paymentMethod: {
      type: String,
      required: false,
    },
    cardInfo: {
      type: Object,
      required: false,
    },
    sellFrom: {
      type: String,
      enum: ['POS', 'SHOP'],
      // default: "SHOP",
    },
    seller: {
      type: String,
      required: false,
    },
    status: {
      type: String,
      enum: ['Pending', 'Processing', 'Delivered', 'Cancel', 'POS-Completed'],
    },
  },
  {
    timestamps: true,
  }
);

module.exports = orderSchema;
